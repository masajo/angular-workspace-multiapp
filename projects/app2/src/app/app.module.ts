import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatButtonModule} from '@angular/material/button';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SecondPageComponent } from './pages/second-page/second-page.component';
import { NavComponent } from './components/nav/nav.component';


// Planteamos unos providers que se puedan compartir
// Estos van a ser sustituidos en los decoradores de los módulos
// Para que AppModule y AppSharedModule tengan los mismos providers
const providers: [] = [
  // Aquí irían los providers de esta app que serán compartidos con app0
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SecondPageComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule
  ],
  providers, // Es lo mismo que poner providers: providers
  bootstrap: [AppComponent]
})
export class AppModule { }

// * Módulo compartido que se usa en el AppRoutingModule de App0
// Cuando queremos compartir un módulo de una aplicación
// Para cargarlo en otra aplicación, necesitamos hacer uso de ModuleWithProviders
// Añadimos un clase módulo que será el módulo compartido para poder cargar
// el contenido de esta aplicación
@NgModule({})
export class App2SharedModule {

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: AppModule,
      providers // providers: providers
    }
  }

}
