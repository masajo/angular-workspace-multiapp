import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SecondPageComponent } from './pages/second-page/second-page.component';

const routes: Routes = [
  {
    path: 'app2/home',
    component: HomePageComponent
  },
  {
    path: 'app2/second',
    component: SecondPageComponent
  },
  {
    path: 'app2',
    redirectTo: 'app2/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
