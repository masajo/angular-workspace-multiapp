import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SecondPageComponent } from './pages/second-page/second-page.component';

const routes: Routes = [
  {
    path: 'app1/home',
    component: HomePageComponent
  },
  {
    path: 'app1/second',
    component: SecondPageComponent
  },
  {
    path: 'app1',
    redirectTo: 'app1/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
